//
//  LHNetworkService.h
//  35iSDK
//
//  Created by 林涛 on 14/11/3.
//  Copyright (c) 2014年 林涛. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OSNetworkRequestConstants.h"
@interface OSNetworkService : NSObject
+ (void)httpRequestWithParam:(NSDictionary *)param
                 requestPath:(NSString *)path
                     success:(SucceedBlock)successBlock
                     failure:(void (^)(NSError *error))failure;

+ (void)yeepayWithParam:(NSDictionary *)param
                success:(SucceedBlock)successBlock
                failure:(void (^)(NSError *error))failure;
+ (BOOL)hasNetWork;

+ (void)startGetRequesturl:(NSString *)urlstring
              competeBlock:(SucceedBlock)block
                   failure:(void (^)(NSError *error))failure;
//+ (BOOL)isEnableWIFI;

//+ (BOOL)isEnable3G;
@end
