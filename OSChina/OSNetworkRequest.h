//
//  OSNetworkRequest.h
//  OSChina
//
//  Created by 林涛 on 14/12/25.
//  Copyright (c) 2014年 林涛. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OSNetworkRequestConstants.h"
@interface OSNetworkRequest : NSObject
+ (void)newListRequestWithCount:(NSString *)count
                        success:(SucceedBlock)successBlock
                        failure:(void (^)(NSError *error))failureBlock;

+ (void)blogListRequestWithCount:(NSString *)type
                         success:(SucceedBlock)successBlock
                         failure:(void (^)(NSError *error))failureBlock;
@end
