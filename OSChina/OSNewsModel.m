//
//  OSNewsModel.m
//  OSChina
//
//  Created by 林涛 on 14/12/25.
//  Copyright (c) 2014年 林涛. All rights reserved.
//

#import "OSInfoModel.h"

@implementation OSInfoModel
- (id)mapAttributes
{
    NSArray *map = @[@{@"authorname":@"authorname"},@{@"authorname":@"author"},
                     @{@"authoruid":@"authoruid"},@{@"authoruid":@"authorid"},
                     @{@"commentCount":@"commentCount"},@{@"documentType":@"documentType"},
                     @{@"identifier":@"id"},@{@"pubDate":@"pubDate"},
                     @{@"title":@"title"},@{@"url":@"url"}
                     ];
    return map;
}
@end
