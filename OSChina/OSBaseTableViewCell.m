//
//  OSBaseTableViewCell.m
//  OSChina
//
//  Created by 林涛 on 14/12/25.
//  Copyright (c) 2014年 林涛. All rights reserved.
//

#import "OSBaseTableViewCell.h"

@implementation OSBaseTableViewCell
+ (UINib *)nib
{
    UINib *nib = [UINib nibWithNibName:NSStringFromClass([self class]) bundle:nil];
    return nib;
}

+ (NSString *)identifier
{
    return @"";
}

- (void)configWith:(id)item
{
    
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
