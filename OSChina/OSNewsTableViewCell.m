//
//  OSInfoTableViewCell.m
//  OSChina
//
//  Created by 林涛 on 14/12/25.
//  Copyright (c) 2014年 林涛. All rights reserved.
//

#import "OSInfoTableViewCell.h"
#import "OSInfoModel.h"
@implementation OSInfoTableViewCell
+ (NSString *)identifier
{
    return @"InfoCell";
}

- (void)configWith:(id)item
{
    self.model = [[OSInfoModel alloc]initWithContent:item];
    self.textLabel.text = self.model .title;
    self.detailTextLabel.text = [NSString stringWithFormat:@"%@ 发布于 %@ (%@评)", self.model.authorname, self.model .pubDate, self.model .commentCount];
}

@end
