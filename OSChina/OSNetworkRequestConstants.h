//
//  OSNetworkRequestConstants.h
//  OSChina
//
//  Created by 林涛 on 14/12/25.
//  Copyright (c) 2014年 林涛. All rights reserved.
//

#ifndef OSChina_OSNetworkRequestConstants_h
#define OSChina_OSNetworkRequestConstants_h

typedef void(^SucceedBlock)(id result);

#define BASE_URL @"http://www.oschina.net"
#define api_news_list @"/action/api/news_list"

#define api_blog_list @"/action/api/blog_list"
#endif
