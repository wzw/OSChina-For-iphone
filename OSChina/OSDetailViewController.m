//
//  OSInfoDetailViewController.m
//  OSChina
//
//  Created by 林涛 on 14/12/26.
//  Copyright (c) 2014年 林涛. All rights reserved.
//

#import "OSInfoDetailViewController.h"

@interface OSInfoDetailViewController ()
@property (strong, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation OSInfoDetailViewController

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSURL *url = [NSURL URLWithString:self.infoModel.url];
    if (url) {
        NSURLRequest *request = [[NSURLRequest alloc]initWithURL:url];
        [self.webView loadRequest:request];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
