//
//  LHNetworkService.m
//  35iSDK
//
//  Created by 林涛 on 14/11/3.
//  Copyright (c) 2014年 林涛. All rights reserved.
//

#import "OSNetworkService.h"
#import <AFNetworking/AFNetworking.h>
#import "OSNetworkRequestConstants.h"
@implementation OSNetworkService

+ (void)httpRequestWithParam:(NSDictionary *)param
                 requestPath:(NSString *)path
                     success:(SucceedBlock)successBlock
                     failure:(void (^)(NSError *error))failure
{
    NSString *url = [BASE_URL stringByAppendingString:path];
    if ([self hasNetWork]) {
        [self startPostRequest:param url:url competeBlock:successBlock failure:failure];
    } else {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"无网络连接,请检查网络" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alert show];
    }
    
}

+ (void)startPostRequest:(NSDictionary *)params
                     url:(NSString *)urlstring
            competeBlock:(SucceedBlock)block
                 failure:(void (^)(NSError *error))failure
{
    
    if (![urlstring length]) {
        return;
    }
    if (![params count] || !params) {
        return;
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFXMLParserResponseSerializer serializer];
    
    [manager POST:urlstring parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        block(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"post request error %@",[error localizedDescription]);
        failure(error);
    }];
}

+ (void)startGetRequesturl:(NSString *)urlstring
              competeBlock:(SucceedBlock)block
                   failure:(void (^)(NSError *error))failure
{
    if (![urlstring length]) {
        return;
    }
    NSLog(@"%@",urlstring);
    AFHTTPRequestOperationManager *manage = [AFHTTPRequestOperationManager manager];
    manage.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manage GET:urlstring parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        block(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(failure);
    }];
}

+ (void)yeepayWithParam:(NSDictionary *)param
                success:(SucceedBlock)successBlock
                failure:(void (^)(NSError *error))failure
{
    if (![param count] || !param) {
        return;
    }
    
    AFHTTPRequestOperationManager *manage = [AFHTTPRequestOperationManager manager];
    manage.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
  
    [manage POST:@"http://pay.35i.com/yeepay/card/req.php?" parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        successBlock(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"post request error %@",[error localizedDescription]);
        failure(error);
    }];
}

//是否wifi
//+ (BOOL)isEnableWIFI {
//    return ([[Reachability reachabilityForLocalWiFi] currentReachabilityStatus] == ReachableViaWiFi);
//}
//
//// 是否3G
//+ (BOOL)isEnable3G {
//    return ([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == ReachableVia3G);
//}

+ (BOOL)hasNetWork {
    //return ([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] != NotReachable);
    
    return YES;
}
@end
