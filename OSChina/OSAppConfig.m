//
//  OSAppConfig.m
//  OSChina
//
//  Created by 林涛 on 14/12/25.
//  Copyright (c) 2014年 林涛. All rights reserved.
//

#import "OSAppConfig.h"
#import "AESCrypt.h"
@implementation OSAppConfig
+ (OSAppConfig *)shareConfig
{
    static id shareObject = nil;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        shareObject = [[[self class]alloc]init];
    });
    return shareObject;
}

-(void)saveUserNameAndPwd:(NSString *)userName andPwd:(NSString *)pwd
{
    NSUserDefaults * settings = [NSUserDefaults standardUserDefaults];
    pwd = [AESCrypt encrypt:pwd password:@"pwd"];
    [settings setObject:userName forKey:@"UserName"];
    [settings setObject:pwd forKey:@"Password"];
    [settings synchronize];
}

- (NSString *)password
{
    NSUserDefaults * settings = [NSUserDefaults standardUserDefaults];
    return [settings objectForKey:@"Password"];
}

- (NSString *)userName
{
    NSUserDefaults * settings = [NSUserDefaults standardUserDefaults];
    return [settings objectForKey:@"UserName"];
}

-(void)saveUID:(int)uid
{
    NSUserDefaults *setting = [NSUserDefaults standardUserDefaults];
    [setting removeObjectForKey:@"UID"];
    [setting setObject:[NSString stringWithFormat:@"%d", uid] forKey:@"UID"];
    [setting synchronize];
}

-(NSInteger)UID
{
    NSUserDefaults * setting = [NSUserDefaults standardUserDefaults];
    NSString *value = [setting objectForKey:@"UID"];
    if (value && [value isEqualToString:@""] == NO)
    {
        return [value intValue];
    }
    else
    {
        return 0;
    }
}

@end
