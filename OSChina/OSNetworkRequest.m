//
//  OSNetworkRequest.m
//  OSChina
//
//  Created by 林涛 on 14/12/25.
//  Copyright (c) 2014年 林涛. All rights reserved.
//

#import "OSNetworkRequest.h"
#import "OSNetworkService.h"
#import <XMLDictionary/XMLDictionary.h>
@interface OSNetworkService()
@end
@implementation OSNetworkRequest
+ (void)newListRequestWithCount:(NSString *)count
                        success:(SucceedBlock)successBlock
                        failure:(void (^)(NSError *error))failureBlock
{
    NSDictionary *param = @{@"catalog"  : @"1",
                            @"pageIndex": @"10",
                            @"pageSize":count
                            };
    [OSNetworkService httpRequestWithParam:param requestPath:api_news_list success:^(id result) {
        if ([result isKindOfClass:[NSXMLParser class]]) {
            NSDictionary *dic = [NSDictionary dictionaryWithXMLParser:result];
            successBlock(dic);
        }
    } failure:^(NSError *error) {
        failureBlock(error);
    }];
}

+ (void)blogListRequestWithCount:(NSString *)type
                        success:(SucceedBlock)successBlock
                        failure:(void (^)(NSError *error))failureBlock
{
    NSDictionary *param = @{@"type"  : type,
                            @"pageIndex": @"1",
                            @"pageSize":@"20"
                            };
    [OSNetworkService httpRequestWithParam:param requestPath:api_blog_list success:^(id result) {
        if ([result isKindOfClass:[NSXMLParser class]]) {
            NSDictionary *dic = [NSDictionary dictionaryWithXMLParser:result];
            successBlock(dic);
        }
    } failure:^(NSError *error) {
        failureBlock(error);
    }];
}

@end
