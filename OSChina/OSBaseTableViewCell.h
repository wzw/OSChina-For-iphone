//
//  OSBaseTableViewCell.h
//  OSChina
//
//  Created by 林涛 on 14/12/25.
//  Copyright (c) 2014年 林涛. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OSBaseTableViewCell : UITableViewCell
+ (NSString *)identifier;
+ (UINib *)nib;
- (void)configWith:(id)item;
@end
