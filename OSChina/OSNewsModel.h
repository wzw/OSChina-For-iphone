//
//  OSNewsModel.h
//  OSChina
//
//  Created by 林涛 on 14/12/25.
//  Copyright (c) 2014年 林涛. All rights reserved.
//  综合信息模型层
//

#import "OSBaseModel.h"
@interface OSInfoModel : OSBaseModel
@property (nonatomic, strong) NSString *authorname;
@property (nonatomic, strong) NSString *authoruid;
@property (nonatomic, strong) NSString *commentCount;
@property (nonatomic, strong) NSString *documentType;
@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) NSString *pubDate;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSDictionary *newsType;
@end
