//
//  FirstViewController.m
//  OSChina
//
//  Created by 林涛 on 14/12/23.
//  Copyright (c) 2014年 林涛. All rights reserved.
//


#import "MJRefresh.h"
#import "OSNetworkRequest.h"
#import "OSInfoTableViewCell.h"
#import "OSInfoViewController.h"
#import "OSTableViewDataSourceObject.h"

typedef enum : NSUInteger {
    OSAnswerType = 0,
    OSBlogType,
    OSReadType,
} OSInfoType;

@interface OSInfoViewController ()<NSXMLParserDelegate,UITabBarControllerDelegate>
@property (assign, nonatomic) BOOL reloading;
@property (assign, nonatomic) OSInfoType infoType;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedController;
@property (strong, nonatomic) NSArray *blogList;
@property (strong, nonatomic) OSTableViewDataSourceObject *tableViewDataSourceObject;
@end

@implementation OSInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.infoType = OSAnswerType;
    [self setup];
}

- (void)setup
{
    self.tabBarItem.title = @"综合";
    TableViewCellConfigureBlock block = ^(OSInfoTableViewCell *cell, id item,NSIndexPath *indexPath){
        [cell configWith:item];
    };
    self.tableViewDataSourceObject = [[OSTableViewDataSourceObject alloc]initWithItems:self.blogList cellIdentifier:[OSInfoTableViewCell identifier] configureCellBlock:block];
    self.tableView.dataSource = self.tableViewDataSourceObject;
    [self.tableView addHeaderWithTarget:self action:@selector(headerRereshing)];
    [self.tableView headerBeginRefreshing];
}

- (NSString *)segmentTitle
{
    return [self.segmentedController titleForSegmentAtIndex:self.infoType];
}

#pragma mark 开始进入刷新状态
- (void)headerRereshing
{
    switch (self.infoType) {
        case OSAnswerType:
        {
            [OSNetworkRequest newListRequestWithCount:@"20" success:^(id result) {
                NSDictionary *dic = result[@"newslist"];
                self.tableViewDataSourceObject.items = dic[@"news"];
                [self refreshTableView];
            } failure:^(NSError *error) {
                NSLog(@"%@",error.localizedDescription);
            }];
        }
            break;
        case OSBlogType:
        {
            [OSNetworkRequest blogListRequestWithCount:@"latest" success:^(id result) {
                NSDictionary *dic = result[@"blogs"];
                self.tableViewDataSourceObject.items = dic[@"blog"];
                [self refreshTableView];
            } failure:^(NSError *error) {
               NSLog(@"%@",error.localizedDescription);
            }];
 
        }
            break;
        case OSReadType:
        {
            [OSNetworkRequest blogListRequestWithCount:@"recommend" success:^(id result) {
                NSDictionary *dic = result[@"blogs"];
                self.tableViewDataSourceObject.items = dic[@"blog"];
                [self refreshTableView];
            } failure:^(NSError *error) {
                NSLog(@"%@",error.localizedDescription);
            }];
        }
            break;
        default:
            break;
    }
}

- (void)refreshTableView
{
    [self.tableView reloadData];
    [self.tableView headerEndRefreshing];
}

- (IBAction)segmentAction:(UISegmentedControl *)sender {
    self.infoType = sender.selectedSegmentIndex;
    [self.tableView headerBeginRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController NS_AVAILABLE_IOS(3_0)
{
    OSInfoTableViewCell *cell = (OSInfoTableViewCell *)[self.tableView cellForRowAtIndexPath:self.tableView.indexPathForSelectedRow];
    [viewController setValue:cell.model forKey:@"infoModel"];
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([sender isKindOfClass:[OSInfoTableViewCell class]]) {
        OSInfoTableViewCell *cell = sender;
        UITabBarController *vc = segue.destinationViewController;
        vc.delegate = self;
        [vc setSelectedIndex:0];
        vc.selectedViewController.tabBarItem.title = [NSString stringWithFormat:@"%@详情",[self segmentTitle]];
        [vc.selectedViewController setValue:cell.model forKey:@"infoModel"];
    }
}


@end
