//
//  OSInfoDetailViewController.h
//  OSChina
//
//  Created by 林涛 on 14/12/26.
//  Copyright (c) 2014年 林涛. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OSInfoModel.h"
@interface OSInfoDetailViewController : UIViewController
@property (strong, nonatomic) OSInfoModel *infoModel;
@end
